// import logo from './logo.svg';
import './App.css';
import Navbar from './Components/Navbar';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import Movies from './Components/Movies';
import WatchList from './Components/WatchList'
import Banner from './Components/Banner'
import Details from './Components/Details';
import TopRated from './Components/TopRated';
import Upcoming from './Components/Upcoming';
import TvShow from './Components/TvShow';
import { useState } from 'react';
function App() {
  const [watchList,setWatchlis] =useState([])
  let handleAddtoWatchList = (movieObj) =>{
    let newWatchList = [...watchList, movieObj]
    setWatchlis(()=>newWatchList)
    console.log("hello", watchList)
  }
  let removeFromWatchlis = (movieObj) =>{
    let rnewWatchList = watchList.filter(items=>{
       return items.is !==movieObj.id
    })
    setWatchlis(()=>rnewWatchList)
  }

  return (
    <>
      <BrowserRouter>
        <Navbar />
        <Routes>
          <Route path="/movies-war" element={<><Banner/> <Movies handleAddtoWatchList={handleAddtoWatchList} removeFromWatchlis={removeFromWatchlis}/></>} />
          <Route path="/watchlist" element={<WatchList watchList= {watchList}/>} />
          <Route path="/detailsPage/:id" element={<Details />} />
          <Route path="/toprated" element={<TopRated />} />
          <Route path="/upcoming" element={<Upcoming />} />
          <Route path="/tv" element={<TvShow />} />
          {/* <Route path="/detailsPage/:id" element={< />} />
          <Route path="/detailsPage/:id" element={<Details />} />
           */}
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
