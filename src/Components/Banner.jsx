import React from "react";

const Banner =(props) =>{
    // console.log(props)
    return(
        // ``
        <>
            {props.banner ?<div className="h-[15vh] md:h-[60vh] w-[40vh] rounded-xl bg-cover bg-center flex item-end ml-20 mt-10" style={{backgroundImage :`url(https://image.tmdb.org/t/p/original/${props.banner})`}}>

            </div>:<div className="h-[20vh] md:h-[75vh] bg-cover bg-center flex item-end" style={{backgroundImage :`url(https://collider.com/wp-content/uploads/the-avengers-movie-poster-banners-03.jpg)` }}>

</div>}
        </>
    )
}
export default Banner