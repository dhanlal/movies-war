import axios from "axios";
import React, {useEffect,useState} from "react";
import { useParams } from "react-router-dom";
import Banner from "./Banner";
const Details = (props) =>{
    const [details, setDetail] = useState([])
    // const userId = props.match.params.id;
    const { id } = useParams();
    useEffect(()=>{
        axios.get(`https://api.themoviedb.org/3/movie/${id}?api_key=c576abfad2882f400d87303aaedc2720&language=en-US`).then(res =>{
            setDetail(res?.data)
            // console.log(res)
        })
     },[id])
    return(
        <>
         
         <div className="flex h-[100%] bg-cover" style={ {backgroundImage:`url(https://image.tmdb.org/t/p/original/${details.backdrop_path})`}}>
         <Banner banner={details?.poster_path}/>
            <div className="mt-10 ml-10 text-white">
                <h2 className="text-2xl">{details?.title} ({details.release_date?.split('-')[0]})</h2>
                <p>{details?.release_date} ({details?.origin_country}) {details?.genres?.map((item)=>{return <span>{item.name},</span>})} . {details.runtime} Minute</p>
                <label className="text-2xl bold">Overview</label>
                <p>{details.overview}</p>
            </div>
         </div>
        </>
    )
}
export default Details