import { Link } from "react-router-dom"
// import Details from "./Details"

const MovieCard = ({moviesObj, handleAddtoWatchList,removeFromWatchlis}) =>{
   const styles ={
    // position: 'relative',
    // top: '100%',
    // left: '10%',
    background: 'lightgray'
   }
//    console.log("dvkndjkbv",moviesObj)
    return(
        <>
            <div  className=" rounded-bl-full flex-col  justify-between items-end">
            {/* className="h-[40vh] w-[150px] bg-center bg-cover rounded-xl hover:scale-110 duratio-300 hover:cursor-pointer items-end" */}
                {/* <span className="relative top-230 left-5 text-wrap bg-black text-white">{props?.moviesObj?.original_title}</span> */}
                {/*  */}
                {/* style={{backgroundImage:`url(https://image.tmdb.org/t/p/original/${props?.moviesObj?.poster_path})`}} */}
                {/* onClick={()=>(handleAddtoWatchList(moviesObj))} */}
                <span  className="items-center rounded-lg bg-gray-900/60 cursor-pointer" style={{position:"relative", top: "25px", left:"137px", "z-index": "1000"}}>
                    &#128525;
                </span>
                <Link to={`/detailsPage/${moviesObj.id}`} className="text-blue-500  pl-3">
                    <img className="hover:scale-105 duratio-300 rounded-tl-xl rounded-tr-xl h-[40vh] w-[160px] bg-center bg-cover" src={"https://image.tmdb.org/t/p/original/"+moviesObj?.poster_path} alt="search" /></Link>
                
                <div className="content h-[16vh] w-[160px] rounded-bl-xl rounded-br-xl" style={styles}>
                    <div className="consensus tight">
                    <div className="outer_ring">
                        <div className="user_score_chart 5de6f6133faba00015133c4d" data-percent="69" data-track-color="#423d0f" data-bar-color="#d2d531">
                        <div className="percent">
                            <span className="icon icon-r69"></span>
                        </div>
                        <canvas height="42" width="42" style={{height: 42, width:42}} ></canvas>
                        </div>
                    </div>
                    </div>

                    <span className="flex-wrap text-center ">
                            <Link to={`/detailsPage/${moviesObj.id}`} className="text-blue-500  pl-3">{moviesObj?.title}</Link>
                        
                    
                    </span>
                    <p className="pl-2">{moviesObj?.release_date}</p>
               </div>
            </div>
            
            
        </>
    )
}
export default MovieCard