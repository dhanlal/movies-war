import React ,{useState,useEffect} from "react"
import MoviewCard from './MovieCard'
import Paginations from './Paginations'
import axios from "axios"
import Spinner from "./Spinner"
const Movies =({handleAddtoWatchList, removeFromWatchlis})=>{
const [movies,setMovies] =useState([])
const [pageNo, setPageNo] =useState(1)
const [loading, setLoading] = useState(false)

const handlePrev =()=>{
    if(pageNo>1)setPageNo(pageNo-1)
}
const handleNext =()=>{
    setPageNo(pageNo+1)
}
    useEffect( ()=>{
        const fetchData = async () => {
        setLoading(()=>true);
        const data = await axios.get(`https://api.themoviedb.org/3/movie/popular?api_key=c576abfad2882f400d87303aaedc2720&language=en-US&page=${pageNo}`);
        try{
        // console.log(res.data?.results)    
            setMovies(()=>data?.data?.results)
        }catch(err) {
            console.log(err)
        } finally{
            setLoading(()=>false);
        }
     }
     fetchData();       
    },[pageNo])
    if (loading) {
        return <Spinner loading={loading}/>
      }
    return(
        <>
        
         <div className="text-2xl  font-bold  background-grey shadow"><span className="p-4 table">Trending Movie</span></div>
         <div className="flex">
         <div className="w-[120vh] m-1">
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
            </div>
         <div className="flex flex-row flex-wrap justify-around gap-4">
           { movies.length>0?  movies?.map((moviesObj)=>{return  <MoviewCard moviesObj={moviesObj} handleAddtoWatchList={handleAddtoWatchList} removeFromWatchlis={removeFromWatchlis}/>})
           :<p>No records Found</p>}
         </div>
         </div>
          <Paginations pageNo={pageNo} handleNext={handleNext} handlePrev={handlePrev}/>
         </>
    )
}
export default Movies