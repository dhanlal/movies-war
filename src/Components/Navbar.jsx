import React from "react";
import Logo from '../MovieLogo.jpg'
import { Link } from "react-router-dom";

const Navbar = ()=>{
    
     return (
        <div className="flex border spece-x-4 items-center pl-3 py-1 bg-black shadow">
             <Link to="/movies-war" className="text-blue-500 text-2xl pl-3"><img className="w-[30px]" src={Logo} alt=""/>
           </Link>
            <div className="flex">
                 <nav>
                    <ul id="menu" className="mainMenu show" >
                        <li className="topmenu">
                        <a href={() => false} className="topmenutxt text-blue-500 text-xl pl-3">Movies</a>
                            <ul class="submenu">
                                    <li className="noTopBorder submenuli ">
                                    <Link to="/movies-war" className="text-blue-500 text-sm pl-3 submenutxt">Popular</Link>
                                    <Link to="/playu" className="text-blue-500 text-sm pl-3 submenutxt">Now Playing</Link>
                                    <Link to="/toprated" className="text-blue-500 text-sm pl-3 submenutxt">Top Rated</Link>
                                    <Link to="/upcoming" className="text-blue-500 text-sm pl-3 submenutxt">Upcoming</Link>
                                </li>
                            </ul>
                        </li>
                       
                        <li className="topmenu">
                        <a href={() => false} className="topmenutxt text-blue-500 text-xl pl-3">TV shows</a>
                            <ul class="submenu">
                                    <li className="noTopBorder submenuli ">
                                    <Link to="/tv" className="text-blue-500 text-sm pl-3 submenutxt">Popular</Link>
                                    <Link to="/nowplaying" className="text-blue-500 text-sm pl-3 submenutxt">Arving Today</Link>
                                    <Link to="/toprated" className="text-blue-500 text-sm pl-3 submenutxt">On The Air</Link>
                                    <Link to="/upcoming" className="text-blue-500 text-sm pl-3 submenutxt">Top Rated</Link>
                                </li>
                            </ul>
                        </li>
                        
                        <li className="topmenu">
                        <a href={() => false} className="topmenutxt text-blue-500 text-xl pl-3">People</a>
                            <ul class="submenu">
                                    <li className="noTopBorder submenuli ">
                                    <Link to="/popularmoview" className="text-blue-500 text-sm pl-3 submenutxt">Popular People</Link>
                                                                        
                                </li>
                            </ul>
                        </li>
                        
                        <li className="topmenu">
                        <Link to="/watchlist" className="topmenutxt text-blue-500 text-xl pl-3 ">Watch List</Link>
                        {/* <a href="Javascript:void(0);" className="topmenutxt text-blue-500 text-xl pl-3">WatchList</a>
                            <ul class="submenu">
                                    <li className="noTopBorder submenuli ">
                                    <Link to="/popularmoview" className="text-blue-500 text-sm pl-3 submenutxt">Popular</Link>
                                    <Link to="/nowplaying" className="text-blue-500 text-sm pl-3 submenutxt">Now Playing</Link>
                                    <Link to="/toprated" className="text-blue-500 text-sm pl-3 submenutxt">Top Rated</Link>
                                    <Link to="/upcomin" className="text-blue-500 text-sm pl-3 submenutxt">Upcoming</Link>
                                </li>
                            </ul> */}
                        </li>
                        </ul>
                    </nav>
                </div>
            </div>
     )
}
export default Navbar;