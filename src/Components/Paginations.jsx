
const Paginations = ({pageNo, handleNext,handlePrev}) =>{
    return(
        <div className="bg-gray-400 p-4 mt-8 flex justify-center">
            <div onClick={handlePrev} className="px-8">Prev</div>
                <div>{pageNo}</div>
            <div onClick={handleNext} className="px-8">Next</div>
        </div>
    )
}

export default Paginations