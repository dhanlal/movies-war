import React from "react";
import { ThreeCircles } from "react-loader-spinner";

const Spinner = (props)=>{
    const styles = {
        position: "absolute",
        top: "40%",
        right: "50%"
    }
    return(
        <ThreeCircles
            visible={props.loading}
            height="100"
            width="100"
            color="#4fa94d"
            ariaLabel="three-circles-loading"
            wrapperStyle={styles}
            wrapperClass=""
        />
    )
}
export default Spinner