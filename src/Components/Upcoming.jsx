import axios from "axios";
import React, { useEffect ,useState} from "react";
import MoviewCard from './MovieCard';
import Paginations from "./Paginations";
const Upcoming = () => {
    const [pageNo, setPageNo] = useState(1)
    const [movies, setMovies] = useState([])
    const handlePrev = () => {
        if (pageNo > 1) setPageNo(pageNo - 1)
    }
    const handleNext = () => {
        setPageNo(pageNo + 1)
    }
    useEffect(() => {
        axios.get(`https://api.themoviedb.org/3/movie/upcoming?api_key=c576abfad2882f400d87303aaedc2720&language=en-US&page=1`).then(res => {
            
            setMovies(res?.data?.results) 
        },[])
    })
    return (
        <><div className="text-2xl font-bold background-grey shadow"><span className="p-4 table">Upcoming Movie</span></div>
        <div className="flex">
            <div className="w-[120vh] m-1">
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
                <div className="m-4 iten-center shadow">Shide bar</div>
            </div>
            <div className="flex flex-row flex-wrap justify-around gap-4">
                {movies?.map((moviesObj) => { return <MoviewCard moviesObj={moviesObj} />; })}
            </div>
        </div><Paginations pageNo={pageNo} handleNext={handleNext} handlePrev={handlePrev} /></>
    )
}
export default Upcoming