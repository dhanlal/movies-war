import React, { useState } from "react"
const Watchlist = ({watchList}) => {
    const [s, sb] = useState(0)
    const [search ,setSearch] = useState('')
    const handleSearch = () =>{
        setSearch(1)
    }
    const sortMovie = () =>{
        sb(2)
        console.log(watchList)
    }
    return (
        <>
        <div className="flex justify-center flex-wrap m-4">
            <div className="bg-blue-400 ml-4 h-[3rem] w-[9rem] items-center flex justify-center rounded-xl">
                    <span>All Movies</span>
            </div>
            <div className="bg-blue-400 ml-4 h-[3rem] w-[9rem] items-center flex justify-center rounded-xl">
                    <span>Action</span>
            </div>
            <div className="bg-blue-400 ml-4 h-[3rem] w-[9rem] items-center flex justify-center rounded-xl">
                    <span>Action</span>
            </div>
            <div className="bg-blue-400 ml-4 h-[3rem] w-[9rem] items-center flex justify-center rounded-xl">
                    <span>Gener</span>
            </div>
        </div>
            <div className="flex justify-center my-4">
                <input className="h-[3rem] w-[18rem] bg-gray-200 outline-none px-4" type="text" placeholder="search here" />

            </div>
            <div className="overflow-hidden rounded-lg border border-gray-200 m-8">
                <table className="w-full text-gray-500 text-center">
                    <thead className="border-b-2">
                        <tr>
                            <th>Name</th>
                            <th>Ratings</th>
                            <th>Popularity</th>
                            <th>Gener</th>
                        </tr>
                    </thead>
                    <body>
                        <tr>

                            <td className="flex item-center px-6 py-4">
                                <img className="h-[6rem] w-[10rem]" src={"https://collider.com/wp-content/uploads/the-avengers-movie-poster-banners-03.jpg"} alt="description"/>
                                <div className="mx-10" onClick={handleSearch}></div>
                            </td>
                            <td>{s}</td>
                            <td>{search}</td>
                            <td>Action</td>
                            <td className="text-red" onClick={sortMovie}>Delete</td>
                        </tr>
                    </body>
                </table>
            </div>
        </>
    )
}

export default Watchlist